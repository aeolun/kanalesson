/**
 * Created with JetBrains PhpStorm.
 * User: Bart
 * Date: 6-4-13
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
var vocabulary = {
	'かに': 'crab',
	'いぬ': 'dog',
	'ねこ': 'cat',
	'つの': 'horn',
	'はし': 'bridge',
	'ひ': 'fire',
	'ふえ': 'flute',
	'へい': 'wall',
	'ほし': 'star',
	'ゆき': 'snow',
	'よる': 'night',
	'さる': 'monkey',
	'きれ': 'cloth',
	'しろ': 'castle',
	'かわ': 'river',
	'せん': 'thousand',
	'くし': 'comb',
	'いけ': 'pond',
	'ここ': 'here',
	'そこ': 'there',
	'まい': 'every-',
	'はな': 'flower',
	'かみ': 'paper',
	'あめ': 'candy',
	'えき': 'station',
	'よこ': 'horizontal',
	'なつ': 'summer',
	'いえ': 'house',
	'うた': 'song',
	'うみ': 'sea',
	'え': 'picture',
	'そと': 'outside'
}

var hiragana = {
	'あ': 'a',
	'い': 'i',
	'う': 'u',
	'え': 'e',
	'お': 'o',
	'か': 'ka',
	'き': 'ki',
	'く': 'ku',
	'け': 'ke',
	'こ': 'ko',
	'さ': 'sa',
	'し': 'shi',
	'す': 'su',
	'せ': 'se',
	'そ': 'so',
	'た': 'ta',
	'ち': 'chi',
	'つ': 'tsu',
	'て': 'te',
	'と': 'to',
	'な': 'na',
	'に': 'ni',
	'ぬ': 'nu',
	'ね': 'ne',
	'の': 'no',
	'は': 'ha',
	'ひ': 'hi',
	'ふ': 'fu',
	'へ': 'he',
	'ほ': 'ho',
	'ま': 'ma',
	'み': 'mi',
	'む': 'mu',
	'め': 'me',
	'も': 'mo',
	'や': 'ya',
	'ゆ': 'yu',
	'よ': 'yo',
	'ら': 'ra',
	'り': 'ri',
	'る': 'ru',
	'れ': 're',
	'ろ': 'ro',
	'わ': 'wa',
	'を': 'wo',
	'ん': 'n'
}

var Game = {
	spawnInterval: null,
	currentStrings: [],
	errorStrings: [],
	stage: null,
	itemLayer: null,
	inputLabel: null,
	liveLabel: null,
	scoreLabel: null,
	noticeLabel: null,
	currentText: '',
	lives: 30,
	score: 0,
	spawnPadding: 100,
	lastAnswered: 0,
	options: null,
	init: function(options) {
		Game.options = options;
		Game.spawnTimeout = setTimeout(Game.spawnString, options.spawnTime);
		Game.tickInterval = setInterval(Game.tick, 250);
		Game.currentStrings = [];
		Game.stage = new Kinetic.Stage({
			container: 'container',
			width: 800,
			height: 800
		});

		Game.itemLayer = new Kinetic.Layer();
		Game.itemLayer.add(Game.spawnInputLabel());
		Game.itemLayer.add(Game.spawnLiveLabel());
		Game.itemLayer.add(Game.spawnScoreLabel());
		Game.itemLayer.add(Game.spawnNoticeLabel());

		Game.stage.add(Game.spawnBackgroundLayer());
		Game.stage.add(Game.itemLayer);

		$(document).keydown(Game.keydown);
		$(document).keypress(Game.keypress);
		$(document).keyup(Game.keyup);
	},
	tick: function() {
		var lost = false;
		var copyStrings = Game.currentStrings;
		copyStrings.forEach(function(item, index) {
			var cuttoff = Math.min((Game.stage.getHeight()/3*1.5)+(Game.stage.getHeight()/3*item.object.getScale().x), Game.stage.getHeight()-96);

			if (item.object.getY() > cuttoff) {
				romaji = Game.asRomaji(item.string);
				Game.animateFailure(romaji);
				var audio = new Audio('sounds/'+romaji+'.mp3');
				audio.play();
				Game.errorStrings.push(item.string);
				Game.spawnExplosion(item.object.getX(), item.object.getY(), item.object.getScale().x, true)
				Game.deleteString(index);
				if (Game.lives > 0)	{
					Game.lives--;
					if (Game.lives == 0) lost = true;
				}
			}
		});
		if(lost == true) {
			var losetext = new Kinetic.Text({
				text: 'You lose! Final Score: '+Game.score,
				x: Game.stage.getWidth()/2-300,
				width:600,
				align:'center',
				y: Game.stage.getHeight()/2,
				fill: 'purple',
				fontSize:48,
				fontStyle:'bold',
				stroke: 'yellow'
			});
			Game.itemLayer.add(losetext);
		}
	},
	spawnExplosion: function(x, y, scale, full) {
		var sheet = {
			explode: [{
				x:0,
				y:0,
				width:96,
				height:96
			},{
				x:96,
				y:0,
				width:96,
				height:96
			},{
				x:96*2,
				y:0,
				width:96,
				height:96
			},{
				x:96*3,
				y:0,
				width:96,
				height:96
			},{
				x:96*4,
				y:0,
				width:96,
				height:96
			}],
			explodefull: [{
				x:96*4,
				y:0,
				width:96,
				height:96
			},{
				x:0,
				y:96,
				width:96,
				height:96
			},{
				x:96,
				y:96,
				width:96,
				height:96
			},{
				x:96*2,
				y:96,
				width:96,
				height:96
			},{
				x:96*3,
				y:96,
				width:96,
				height:96
			},{
				x:96*4,
				y:96,
				width:96,
				height:96
			},{
				x:0,
				y:96*2,
				width:96,
				height:96
			},{
				x:96,
				y:96*2,
				width:96,
				height:96
			},{
				x:96*2,
				y:96*2,
				width:96,
				height:96
			},{
				x:96*3,
				y:96*2,
				width:96,
				height:96
			},{
				x:96*4,
				y:96*2,
				width:96,
				height:96
			}]
		}
		var image = new Image();

		image.onload = function() {
			var sprite = new Kinetic.Sprite({
				x: x-8,
				y: y-30,
				scale: scale,
				image: image,
				animation:(full ? 'explodefull' : 'explode'),
				animations:sheet,
				frameRate: 14,
				index:0
			});

			Game.itemLayer.add(sprite);
			sprite.afterFrame((full ? 10 : 4), function() {
				sprite.remove();
			})
			sprite.start();
		}
		image.src = 'explosion.png';
	},
	spawnInputLabel: function() {
		var label = new Kinetic.Text({
			x: Game.stage.getWidth()/2-100,
			width: 200,
			align: 'center',
			y: Game.stage.getHeight()/2,
			text: '',
			fill: 'red',
			fontSize:'26'
		});
		var stringAnimation = new Kinetic.Animation(function(frame) {
			label.setText(Game.currentText);
		}, Game.itemLayer);
		stringAnimation.start();
		Game.inputLabel = label;
		return label;
	},
	spawnNoticeLabel: function() {
		var label = new Kinetic.Text({
			x: 0,
			width: 800,
			align: 'center',
			y: 800,
			text: '',
			fill: 'red',
			fontSize:'20'
		});
		var stringAnimation = new Kinetic.Animation(function(frame) {

		}, Game.itemLayer);
		stringAnimation.start();
		Game.noticeLabel = label;
		return label;
	},
	spawnLiveLabel: function() {
		var label = new Kinetic.Text({
			x: Game.stage.getWidth()/2-150,
			width: 300,
			align: 'center',
			y: Game.stage.getHeight()-100,
			text: '',
			fill: 'red',
			fontSize:'48',
			fontStyle: 'bold',
			stroke: '#F55',
			strokeWidth: 1
		});
		var stringAnimation = new Kinetic.Animation(function(frame) {
			label.setText(Game.lives);
		}, Game.itemLayer);
		stringAnimation.start();
		Game.liveLabel = label;
		return label;
	},
	spawnScoreLabel: function() {
		var label = new Kinetic.Text({
			x: Game.stage.getWidth()-250,
			width: 200,
			align: 'right',
			y: 50,
			text: '',
			fill: 'green',
			fontSize:'26'
		});
		var stringAnimation = new Kinetic.Animation(function(frame) {
			label.setText(Game.score);
		}, Game.itemLayer);
		stringAnimation.start();
		Game.scoreLabel = label;
		return label;
	},
	spawnBackgroundLayer: function() {
		var backgroundLayer = new Kinetic.Layer();
		var image = new Image();
		image.onload = function() {
			var backgroundImage = new Kinetic.Image({
				image: image,
				x:0,
				y:0
			});
			backgroundLayer.add(backgroundImage);
		}
		image.src = 'shinjuku.jpg';

		var animation = new Kinetic.Animation(function(frame) {

		}, backgroundLayer);
		animation.start();
		return backgroundLayer;
	},
	getImage: function(url, options) {
		var image = new Image();
		var imageObj = new Kinetic.Image(options);
		image.onload = function() {
			imageObj.setImage(image);
			imageObj.draw();
		}
		image.src = url;
		return imageObj;
	},
	spawnString: function() {
		console.log('add string');
		var keys = Object.keys(vocabulary);
		if (Game.errorStrings.length > 0) {
			var index = Math.floor(Math.random()*Game.errorStrings.length);
			var stringText = Game.errorStrings[index];
			Game.errorStrings.splice(index, 1)
		} else {
			var stringText = keys[Math.floor(Math.random()*keys.length)];
		}
		console.log(stringText);

		var scale = Math.random()+0.4;
		var group = new Kinetic.Group({
			x: Math.round(Math.random()*(Game.stage.getWidth()-2*Game.spawnPadding))+Game.spawnPadding,
			y: -30,
			scale: scale
		});
		var image = Game.getImage('bomb.png', {
			x:0,
			y:0
		});
		var string = new Kinetic.Text({
			x: 30,
			y: 0,
			text: stringText,
			fill: 'red',
			stroke: 'black',
			fontSize:'30',
			strokeWidth: 1
		});
		group.add(image);
		group.add(string);

		var stringAnimation = new Kinetic.Animation(function(frame) {
			group.setY(group.getY()+(Game.options.bombSpeed*scale));
		}, Game.itemLayer);
		stringAnimation.start();
		Game.currentStrings.push({
			string: stringText,
			object: group,
			animation: stringAnimation,
			created: new Date().getTime()
		});
		Game.itemLayer.add(group);
		Game.stage.draw();
		Game.spawnTimeout = setTimeout(Game.spawnString, stringText.length*Game.options.spawnTime+(Game.options.spawnTime/2)*Game.currentStrings.length);
	},
	deleteString: function(index) {
		var item = Game.currentStrings[index];

		item.object.remove();
		item.animation.stop();
		delete item.animation;

		Game.currentStrings.splice(index, 1);
	},
	keydown: function(event) {
		if (event.which == 8) { event.preventDefault();return false; }
		
	},
	keypress: function(event) {
		var charTyped = String.fromCharCode(event.which);
		Game.currentText += charTyped;
		Game.inputLabel.setText(Game.currentText);
		Game.checkStrings();
		event.preventDefault();
		return false;
	},
	keyup: function(event) {
		if (event.which == 8) {
			Game.currentText = '';
		}
		event.preventDefault();
		event.stopPropagation();
		return false;
	},
	asRomaji: function(string) {
		var romaji = '';
		for(var i = 0; i<string.length; i++) {
			romaji += hiragana[string[i]];
		}
		return romaji;
	},
	checkStrings: function() {
		var copyStrings = Game.currentStrings;
		copyStrings.forEach(function(item, index) {

			var romaji = Game.asRomaji(item.string);

			if (romaji == Game.currentText.toLowerCase() && Game.lives > 0) {
				console.log('match '+index);
				console.log(Game.currentStrings.length);
				var timedifference = new Date().getTime() - Game.lastAnswered;
				Game.lastAnswered = new Date().getTime();
				console.log('took '+timedifference)
				Game.score++;
				Game.playWord(item.string);
				Game.spawnExplosion(item.object.getX(), item.object.getY(), item.object.getScale().x, false)
				Game.deleteString(index);
				Game.animateFailure(vocabulary[item.string], true);

				console.log(Game.currentStrings.length);
				Game.currentText = '';
				//Game.animateSuccess(timedifference, romaji.length*500)
			}
		});
	},
	playWord: function(string) {
		var romaji = hiragana[string.substr(0,1)];
		var audio = new Audio('sounds/'+romaji+'.mp3');
		audio.addEventListener( "ended", function() {
			Game.playWord(string.substr(1));
		}, false);
		audio.play();
	},
	animateFailure: function(text, correct) {
		Game.noticeLabel.setText(text);
		Game.noticeLabel.setFontSize(20);
		Game.noticeLabel.setFill(correct?'green':'red');
		Game.noticeLabel.setOpacity(0);

		Game.noticeLabel.transitionTo({
			opacity: 1,
			y:400,
			duration:0.5,
			fontSize: 144,
			easing: 'ease-out',
			callback: function() {
				Game.noticeLabel.transitionTo({
					opacity: 0,
					y:800,
					duration:0.5,
					fontSize: 72,
					easing: 'ease-in',
					callback: function() {

					}
				})
			}
		})
	}
}

$(document).ready(function() {
	Game.init({
		spawnTime: 1000,
		bombSpeed: 2
	})
})